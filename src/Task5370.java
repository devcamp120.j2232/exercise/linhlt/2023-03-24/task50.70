import s50.Time;
public class Task5370 {
    public static void main(String[] args) throws Exception {
        Time time1= new Time(14, 12, 59);
        time1.setTime(14, 12, 59);
        System.out.println("Time 1: " + time1);
        Time time2= new Time(7, 45, 1);
        time2.setTime(7, 45, 1);
        System.out.println("Time 2: " + time2);
       
        time1.nextSecond();
        time2.previousSecond();
        System.out.println("Task 4 - New Time 1: " + time1);
        System.out.println("Task 4 - New Time 2: " + time2);


    }
}
